using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();
app.UseStaticFiles(new StaticFileOptions()
{
    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "MyFiles")),
    RequestPath = "/MyFiles"

});


app.UseRouting();

app.UseAuthorization();

//conventional Routing
app.MapControllerRoute(
    name: "example",
    pattern: "studentportal/{controller=Student}/{action=Index}/{id?}"
);
app.MapControllerRoute(
    name: "news",
    pattern: "news/{id?}",
    defaults: new { controller="News", action="Read"}
);


app.Run();
