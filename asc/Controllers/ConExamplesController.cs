﻿using Microsoft.AspNetCore.Mvc;
//using System.Web.Mvc;

namespace asc.Controllers
{
    public class ConExamplesController : Controller
    {

        [AcceptVerbs("POST","GET")]       //actionverbs
        [ActionName("Hi")]
        public ContentResult sendGreetings()
        {
            return Content("ASC");
        }

        [HttpGet]
        public IActionResult one()
        {
            return Content("One");
        }

        [HttpGet]
        public IActionResult two()
        {
            return Content("One");
        }



        [NonAction]
        public int calculatesum(int a, int b)
        {
            return a + b;               
        }


    }
}
