﻿using Microsoft.AspNetCore.Mvc;

namespace asc.Controllers
{
    [Route("[controller]/[action]")]    
    public class StudentController : Controller
    {
        
        public string Index()   //function, method, ACTION
        {
            return "ASC from Index ";
        }

        [Route("reg")]    
        [Route("noradin/students/add")]
        public string Register(int id)
        {
            //register student in database
            return $"Studetn registered with id {id}";
        }
    }
}
