﻿using Microsoft.AspNetCore.Mvc;

namespace asc.Controllers
{
    public class IactionExample : Controller
    {
        public IActionResult giveExample(int id)
        {
            if (id == 1)
            {
                return View();
            }else if(id == 2) {
                return Content("ASC, From Give Example Action");
            }else 
            {
                var obj = new { id = 120, name = "Abdulahi", mobile = "252634588899" };
                return Json(obj);   
            }

        }
    }
}
