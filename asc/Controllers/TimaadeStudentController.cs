﻿using asc.Models;
using Microsoft.AspNetCore.Mvc;

namespace asc.Controllers
{
    public class TimaadeStudentController : Controller
    {
        public List<student> AllStudents;

        public TimaadeStudentController()
        {
            AllStudents= new List<student>();   
            AllStudents.Add(new student() { Id=100, Name="Jama Ali", ClassName="2A", Email="email@email.com", Mobile="+252634448899" });
            AllStudents.Add(new student() { Id = 101, Name = "Osman Ali", ClassName = "3A", Email = "email@email.com", Mobile = "+252634442299" });
            AllStudents.Add(new student() { Id = 102, Name = "Khadar Ali", ClassName = "2A", Email = "email@email.com", Mobile = "+252634148899" });
            AllStudents.Add(new student() { Id = 103, Name = "Amina Ali", ClassName = "2D", Email = "email@email.com", Mobile = "+252634445999" });
        }


        //action
        public IActionResult getAll()
        {
            return View(AllStudents);
        }
        public IActionResult ViewStudent(int? id)
        {
            student _model=new student();
            try
            {                
                foreach(student item in AllStudents)
                {
                    if (item.Id == id)
                    {
                        _model= item;   
                    }
                }                
            }
            catch (Exception ex)
            {

            }
            return View(_model);
        }
    }
}
