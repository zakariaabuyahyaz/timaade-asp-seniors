﻿using Microsoft.AspNetCore.Mvc;

namespace asc.Controllers
{
    public class sessionmanagementexamples : Controller
    {
        public IActionResult Index()
        {
            //example of simple viewdata
            ViewData["user"] = "jimcale";
            ViewData["datattime"] = DateTime.Now.ToString();
            ViewData["mobile"] = "252636252";

            //example of list in viewdata
            string[] name = { "Abuyahaya", "Jimcaale", "Ayaanle", "ZamZam" };
            ViewData["studentlist"] = name;

            //examples of viewbag
            ViewBag.UserID = 20;

            return View();
        }

       
    }
}
