﻿using Microsoft.AspNetCore.Mvc;

namespace asc.Controllers
{
    public class NewsController : Controller
    {
        public string Read(int id)
        {
            if (id == 1)
            {
                return $"Government of Somaliland has opened DPWorld Port. News ID: {id}";
            }else if(id == 2) {
                return $"Ethiopian Launched new system on air. News ID: {id}";
            }
            else
            {
                return $"You are reading general news. News ID: {id}";
            }
        }
    }
}
