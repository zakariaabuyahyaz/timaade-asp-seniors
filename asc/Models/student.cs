﻿namespace asc.Models
{
    public class student
    {
        //properties  / field  / columns 
        public int Id { get; set; } 
        public string Name { get; set; }
        public string Mobile { get; set; }

        public string Email { get; set; }
        public string ClassName { get; set; }

    }
}
