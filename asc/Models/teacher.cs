﻿namespace asc.Models
{
    public class teacher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Course { get; set; }
        public string Address { get; set; }

    }
}
